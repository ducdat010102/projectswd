﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Type = Domain.Entities.Type;

namespace Infrastructures.FluentAPIs
{
    public class TypeConfig : IEntityTypeConfiguration<Type>
    {
        public void Configure(EntityTypeBuilder<Type> builder)
        {
            builder.HasKey(x => x.Id);
            builder.HasOne<Product>(s => s.Product)
                .WithMany(s => s.Types)
                .HasForeignKey(fk => fk.ProductId);
        }
    }
}
