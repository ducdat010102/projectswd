﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.FluentAPIs
{
    public class OrderdetailConfig : IEntityTypeConfiguration<OrderDetail>
    {
        public void Configure(EntityTypeBuilder<OrderDetail> builder)
        {
            builder.HasKey(x => x.Id);

            builder.HasOne<Order>(s => s.Order)
                .WithMany(s => s.OrderDetails)
                .HasForeignKey(fk => fk.OrderId);

            builder.HasOne<Product>(s => s.Product)
                .WithMany(s => s.OrderDetails)
                .HasForeignKey(s => s.ProductId);
        }
    }
}
