﻿using Application.Commos;
using Application.Repositories;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Infrastructures.Repositories
{
    public class ServiceRepository : GenericRepository<Service>, IServiceRepository
    {
        private readonly AppDBContext _dbContext;
        public ServiceRepository(AppDBContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<Pagination<Service>> GetServiceByName(string ServiceName, int pageNumber = 0, int pageSize = 10)
        {
            var itemCount = await _dbContext.Services.CountAsync();
            var items = await _dbContext.Services.Where(x => x.ServiceName.Contains(ServiceName))
                                    .OrderByDescending(x => x.CreationDate)
                                    .Skip(pageNumber * pageSize)
                                    .Take(pageSize)
                                    .AsNoTracking()
                                    .ToListAsync();

            var result = new Pagination<Service>()
            {
                PageIndex = pageNumber,
                PageSize = pageSize,
                TotalItemsCount = itemCount,
                Items = items,
            };
            return result;
        }
        public async Task<Pagination<Service>> GetServiceByOrderDetailId(Guid OrderDetailId, int pageNumber = 0, int pageSize = 10)
        {
            var itemCount = await _dbContext.Services.CountAsync();
            var items = await _dbContext.Services.Where(x => x.OrderDetailId.Equals(OrderDetailId))
                                    .OrderByDescending(x => x.CreationDate)
                                    .Skip(pageNumber * pageSize)
                                    .Take(pageSize)
                                    .AsNoTracking()
                                    .ToListAsync();

            var result = new Pagination<Service>()
            {
                PageIndex = pageNumber,
                PageSize = pageSize,
                TotalItemsCount = itemCount,
                Items = items,
            };

            return result;
        }
    }
}
