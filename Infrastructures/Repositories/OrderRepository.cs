﻿using Application.Commos;
using Application.Repositories;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Repositories
{
    public class OrderRepository : GenericRepository<Order>, IOrderRepository
    {
        private readonly AppDBContext _dbContext;
        public OrderRepository(AppDBContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }

        

        public async Task<Pagination<Order>> GetOrderByUserId(Guid UserId, int pageNumber = 0, int pageSize = 10)
        {
            var itemCount = await _dbContext.Orders.CountAsync();
            var items = await _dbContext.Orders.Where(x => x.UserId == UserId)
                                    .Skip(pageNumber * pageSize)
                                    .Take(pageSize)
                                    .AsNoTracking()
                                    .ToListAsync();

            var result = new Pagination<Order>()
            {
                PageIndex = pageNumber,
                PageSize = pageSize,
                TotalItemsCount = itemCount,
                Items = items,
            };

            return result;
        }

        

        
    }
}
