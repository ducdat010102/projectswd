﻿using Application.Commos;
using Application.Repositories;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;


namespace Infrastructures.Repositories
{
    public class BrandRepository : GenericRepository<Brand>, IBrandRepository
    {
        private readonly AppDBContext _dbContext;
        public BrandRepository(AppDBContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<Pagination<Brand>> GetBrandByName(string BrandName, int pageNumber = 0, int pageSize = 10)
        {
            var itemCount = await _dbContext.Brands.CountAsync();
            var items = await _dbContext.Brands.Where(x => x.BrandName.Contains(BrandName))
                                    .OrderByDescending(x => x.CreationDate)
                                    .Skip(pageNumber * pageSize)
                                    .Take(pageSize)
                                    .AsNoTracking()
                                    .ToListAsync();

            var result = new Pagination<Brand>()
            {
                PageIndex = pageNumber,
                PageSize = pageSize,
                TotalItemsCount = itemCount,
                Items = items,
            };
            return result;
        }
    }
}
