﻿using Application.Commos;
using Application.Repositories;
using Domain.Base;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Repositories
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : BaseEntity
    {
        protected DbSet<TEntity> _dbSet;
        public GenericRepository(AppDBContext appDBContext)
        {
            _dbSet = appDBContext.Set<TEntity>();
        }
        public async Task AddAsync(TEntity entity)
        {
            await _dbSet.AddAsync(entity);
        }
        public async Task AddRangeAsync(List<TEntity> entities)
        {
            await _dbSet.AddRangeAsync(entities);
        }
        public Task<List<TEntity>> GetAllAsync() => _dbSet.ToListAsync();
        public async Task<TEntity?> GetByIdAsync(Guid? id)
        {
            var result = await _dbSet.FirstOrDefaultAsync(x => x.Id == id);
            return result;
        }
        public void UpdateRange(List<TEntity> entities)
        {
            _dbSet.UpdateRange(entities);
        }
        public void Update(TEntity entity)
        {
            _dbSet.Update(entity);
        }
        public void Approve(TEntity entity)
        {
            _dbSet.Update(entity);
        }
        public async Task<Pagination<TEntity>> ToPagination(int pageIndex = 0, int pageSize = 10)
        {
            var itemCount = await _dbSet.CountAsync();
            var items = await _dbSet.OrderByDescending(x => x.Id)
                                    .Skip(pageIndex * pageSize)
                                    .Take(pageSize)
                                    .AsNoTracking()
                                    .ToListAsync();

            var result = new Pagination<TEntity>()
            {
                PageIndex = pageIndex,
                PageSize = pageSize,
                TotalItemsCount = itemCount,
                Items = items,
            };

            return result;
        }
        public async Task<List<TEntity>> GetEntitiesByIdsAsync(List<Guid?> Ids) => await _dbSet.Where(x => Ids.Contains(x.Id)).ToListAsync();
    }

}
