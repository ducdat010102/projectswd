﻿using Application.Commos;
using Application.Repositories;
using Microsoft.EntityFrameworkCore;
using Type = Domain.Entities.Type;


namespace Infrastructures.Repositories
{
    public class TypeRepository : GenericRepository<Type>, ITypeRepository
    {
        private readonly AppDBContext _dbContext;
        public TypeRepository(AppDBContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<Pagination<Type>> GetTypeByName(string TypeName, int pageNumber = 0, int pageSize = 10)
        {
            var itemCount = await _dbContext.Products.CountAsync();
            var items = await _dbContext.Types.Where(x => x.TypeName.Contains(TypeName))
                                    .OrderByDescending(x => x.CreationDate)
                                    .Skip(pageNumber * pageSize)
                                    .Take(pageSize)
                                    .AsNoTracking()
                                    .ToListAsync();

            var result = new Pagination<Type>()
            {
                PageIndex = pageNumber,
                PageSize = pageSize,
                TotalItemsCount = itemCount,
                Items = items,
            };
            return result;
        }
    }
}
