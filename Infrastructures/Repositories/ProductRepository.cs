﻿using Application.Commos;
using Application.Repositories;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Infrastructures.Repositories
{
    public class ProductRepository : GenericRepository<Product>, IProductRepository
    {
        private readonly AppDBContext _dbContext;
        public ProductRepository(AppDBContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<Pagination<Product>> GetProductByName(string ProductName, int pageNumber = 0, int pageSize = 10)
        {
            var itemCount = await _dbContext.Products.CountAsync();
            var items = await _dbContext.Products.Where(x => x.ProductName.Contains(ProductName))
                                    .OrderByDescending(x => x.CreationDate)
                                    .Skip(pageNumber * pageSize)
                                    .Take(pageSize)
                                    .AsNoTracking()
                                    .ToListAsync();

            var result = new Pagination<Product>()
            {
                PageIndex = pageNumber,
                PageSize = pageSize,
                TotalItemsCount = itemCount,
                Items = items,
            };
            return result;
        }
        public async Task<Pagination<Product>> GetProductByBrandId(Guid BrandId, int pageNumber = 0, int pageSize = 10)
        {
            var itemCount = await _dbContext.Products.CountAsync();
            var items = await _dbContext.Products.Where(x => x.BrandId.Equals(BrandId))
                                    .OrderByDescending(x => x.CreationDate)
                                    .Skip(pageNumber * pageSize)
                                    .Take(pageSize)
                                    .AsNoTracking()
                                    .ToListAsync();

            var result = new Pagination<Product>()
            {
                PageIndex = pageNumber,
                PageSize = pageSize,
                TotalItemsCount = itemCount,
                Items = items,
            };

            return result;
        }
    }

}
