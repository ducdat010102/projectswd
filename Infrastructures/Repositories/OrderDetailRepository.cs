﻿using Application.Commos;
using Application.Repositories;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;


namespace Infrastructures.Repositories
{

    public class OrderDetailRepository : GenericRepository<OrderDetail>, IOrderDetailRepository
    {
        private readonly AppDBContext _dbContext;
        public OrderDetailRepository(AppDBContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<Pagination<OrderDetail>> GetOrderDetailByOrderId(Guid OrderId, int pageNumber = 0, int pageSize = 10)
        {
            var itemCount = await _dbContext.OrderDetails.CountAsync();
            var items = await _dbContext.OrderDetails.Where(x => x.OrderId.Equals(OrderId))
                                    .OrderByDescending(x => x.CreationDate)
                                    .Skip(pageNumber * pageSize)
                                    .Take(pageSize)
                                    .AsNoTracking()
                                    .ToListAsync();

            var result = new Pagination<OrderDetail>()
            {
                PageIndex = pageNumber,
                PageSize = pageSize,
                TotalItemsCount = itemCount,
                Items = items,
            };

            return result;
        }
        public async Task<Pagination<OrderDetail>> GetOrderDetailByProductId(Guid ProductId, int pageNumber = 0, int pageSize = 10)
        {
            var itemCount = await _dbContext.OrderDetails.CountAsync();
            var items = await _dbContext.OrderDetails.Where(x => x.ProductId.Equals(ProductId))
                                    .OrderByDescending(x => x.CreationDate)
                                    .Skip(pageNumber * pageSize)
                                    .Take(pageSize)
                                    .AsNoTracking()
                                    .ToListAsync();

            var result = new Pagination<OrderDetail>()
            {
                PageIndex = pageNumber,
                PageSize = pageSize,
                TotalItemsCount = itemCount,
                Items = items,
            };

            return result;
        }
    }
}