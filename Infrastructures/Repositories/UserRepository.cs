﻿using Application.Interfaces;
using Application.Repositories;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Repositories
{
    public class UserRepository : GenericRepository<User>, IUserRepository
    {
        private readonly AppDBContext _dbContext;
        public UserRepository(AppDBContext dbContext) : base (dbContext) 
        {
            _dbContext = dbContext;
        }

        public bool GetLoginUser(string email, string password)
        {
            var a = _dbContext.Users.Where(x => x.Email == email && x.Password == password);
            if (a.Any())
            {
                return true;
            }
            else
            {
                return false;
            }
        }
           
        public async Task<User?> GetUserByEmail(string email) => _dbContext.Users.FirstOrDefault(x => x.Email == email);
    }
}
