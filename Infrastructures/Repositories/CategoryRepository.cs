﻿using Application.Commos;
using Application.Repositories;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Repositories
{
    public class CategoryRepository : GenericRepository<Category>, ICategoryRepository
    {
        private readonly AppDBContext _dbContext;
        public CategoryRepository(AppDBContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<Pagination<Category>> GetCategoryByName(string CategoryName, int pageNumber = 0, int pageSize = 10)
        {
            var itemCount = await _dbContext.Categories.CountAsync();
            var items = await _dbContext.Categories.Where(x => x.CategoryName.Contains(CategoryName))
                                    .OrderByDescending(x => x.CreationDate)
                                    .Skip(pageNumber * pageSize)
                                    .Take(pageSize)
                                    .AsNoTracking()
                                    .ToListAsync();

            var result = new Pagination<Category>()
            {
                PageIndex = pageNumber,
                PageSize = pageSize,
                TotalItemsCount = itemCount,
                Items = items,
            };
            return result;
        }
        public async Task<Pagination<Category>> GetCategoryByProductId(Guid ProductId, int pageNumber = 0, int pageSize = 10)
        {
            var itemCount = await _dbContext.Categories.CountAsync();
            var items = await _dbContext.Categories.Where(x => x.ProductId.Equals(ProductId))
                                    .OrderByDescending(x => x.CreationDate)
                                    .Skip(pageNumber * pageSize)
                                    .Take(pageSize)
                                    .AsNoTracking()
                                    .ToListAsync();

            var result = new Pagination<Category>()
            {
                PageIndex = pageNumber,
                PageSize = pageSize,
                TotalItemsCount = itemCount,
                Items = items,
            };

            return result;
        }
    }
}
