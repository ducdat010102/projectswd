﻿using Application.Repositories;
using Applications;


namespace Infrastructures
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDBContext _appDBContext;
        private readonly IProductRepository _productRepository;
        private readonly IBrandRepository _brandRepository;
        private readonly IUserRepository _userRepository;
        private readonly ICategoryRepository _categoryRepository;
        private readonly ITypeRepository _typeRepository;
        private readonly IOrderRepository _orderRepository;
        private readonly IOrderDetailRepository _orderDetailRepository;
        private readonly IServiceRepository _serviceRepository;
        public UnitOfWork(AppDBContext appDBContext,
            IProductRepository productRepository,
            IBrandRepository brandRepository,
            IUserRepository userRepository,
            ICategoryRepository categoryRepository,
            ITypeRepository typeRepository,
            IOrderRepository orderRepository,
            IOrderDetailRepository orderDetailRepository,
            IServiceRepository serviceRepository)
        {
            _appDBContext = appDBContext;
            _productRepository = productRepository;
            _brandRepository = brandRepository;
            _userRepository = userRepository;
            _categoryRepository = categoryRepository;
            _typeRepository = typeRepository;
            _orderRepository = orderRepository;
            _orderDetailRepository = orderDetailRepository;
            _serviceRepository = serviceRepository;
        }
        public IProductRepository ProductRepository => _productRepository;
        public IBrandRepository BrandRepository => _brandRepository;
        public IUserRepository UserRepository => _userRepository;
        public ICategoryRepository CategoryRepository => _categoryRepository;
        public ITypeRepository TypeRepository => _typeRepository;
        public IOrderRepository OrderRepository => _orderRepository;
        public IOrderDetailRepository OrderDetailRepository => _orderDetailRepository;
        public IServiceRepository ServiceRepository => _serviceRepository;
        public async Task<int> SaveChangeAsync() => await _appDBContext.SaveChangesAsync();
        
    }
}
