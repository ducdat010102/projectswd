﻿using Application.Commos;
using Application.ViewModels.BrandViewModels;
using Application.ViewModels.CategoryViewModels;
using Application.ViewModels.OrderDetailViewModels;
using Application.ViewModels.OrderViewModels;
using Application.ViewModels.ProductViewModels;
using Application.ViewModels.ServiceViewModels;
using Application.ViewModels.TypeViewModels;
using Application.ViewModels.UserViewModels;
using AutoMapper;
using Domain.Entities;
using Type = Domain.Entities.Type;



namespace Infrastructures.Mappers
{
    public class MapperConfig : Profile
    {
        public MapperConfig() 
        {
            CreateMap<ProductViewModels, Product>().ReverseMap();
            CreateMap<CreateProductViewModels, Product>().ReverseMap();
            CreateMap<UpdateProductViewModels, Product>().ReverseMap();

            CreateMap<BrandViewModels, Brand>().ReverseMap();
            CreateMap<CreateBrandViewModels, Brand>().ReverseMap();
            CreateMap<UpdateBrandViewModels, Brand>().ReverseMap();

            CreateMap<CreateCategoryViewModel, Category>().ReverseMap();
            CreateMap<CategoryViewModel, Category>().ReverseMap();
            CreateMap<UpdateCategoryViewModel, Category>().ReverseMap();

            CreateMap<CreateTypeViewModel, Type>().ReverseMap();
            CreateMap<TypeViewModel,  Type>().ReverseMap();
            CreateMap<UpdateTypeViewModel, Type>().ReverseMap();

            CreateMap<UserViewModel, User>();

            CreateMap<CreateOrderViewModel, Order>().ReverseMap();
            CreateMap<UpdateOrderViewModel, Order>().ReverseMap();
            CreateMap<OrderViewModel, Order>().ReverseMap();

            CreateMap<OrderDetailViewModel, OrderDetail>().ReverseMap();
            CreateMap<CreateOrderDetailViewModel, OrderDetail>().ReverseMap();
            CreateMap<UpdateOrderDetailViewModel, OrderDetail>().ReverseMap();

            CreateMap<ServiceViewModel, Service>().ReverseMap();
            CreateMap<CreateServiceViewModel, Service>().ReverseMap();
            CreateMap<UpdateServiceViewModel, Service>().ReverseMap();

            CreateMap(typeof(Pagination<>), typeof(Pagination<>));

        }
    }
}
