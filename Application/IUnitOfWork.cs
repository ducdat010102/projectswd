﻿using Application.Repositories;

namespace Applications
{
    public interface IUnitOfWork
    {
        public IProductRepository ProductRepository { get; } 
        public IBrandRepository BrandRepository { get; }
        public IUserRepository UserRepository { get; }
        public ICategoryRepository CategoryRepository { get; }
        public ITypeRepository TypeRepository { get; }
        public IOrderRepository OrderRepository { get; }
        public IOrderDetailRepository OrderDetailRepository { get; }
        public IServiceRepository ServiceRepository { get; }
        public Task<int> SaveChangeAsync();
    }
}
