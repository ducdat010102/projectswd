﻿using Application.Commos;
using Application.Interfaces;
using Application.ViewModels.UserViewModels;
using Applications;
using Applications.ViewModels.Response;
using AutoMapper;
using Domain.Entities;
using Domain.Enum;
using System.Net;

namespace Application.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public UserService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public async Task<Pagination<UserViewModel>> GetAllUsers(int pageIndex = 0, int pageSize = 10)
        {
            var users = await _unitOfWork.UserRepository.ToPagination(pageIndex, pageSize);
            return _mapper.Map<Pagination<UserViewModel>>(users);
        }
        public async Task<Response> AddUser(CreateUserViewModel createUserViewModel)
        {
            var entity = await _unitOfWork.UserRepository.GetUserByEmail(createUserViewModel.Email);
            if (entity is not null)
                return new Response(HttpStatusCode.BadRequest,
                    $"The account with email {createUserViewModel.Email} already exists in the system");
            var user = new User()
            {
                UserName = createUserViewModel.UserName,
                Address = createUserViewModel.Address,
                Email = createUserViewModel.Email,
                Phone = createUserViewModel.Phone,
                Password = createUserViewModel.Password,
                Status = Status.Enable,
            };
            await _unitOfWork.UserRepository.AddAsync(user);
            var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;
            return !isSuccess
                ? new Response(HttpStatusCode.Conflict, "Create user failed!!")
                : new Response(HttpStatusCode.OK, "ok");
        }

        public bool GetLoginUser(UserLoginViewModel userLoginViewModel)
        {
            return  _unitOfWork.UserRepository.GetLoginUser(userLoginViewModel.Email, userLoginViewModel.Password);
        }
        //public async Task<Response> Login(UserLoginViewModel userLoginViewModel)
        //{
        //}
    }
}
