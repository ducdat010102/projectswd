﻿using Application.Commos;
using Application.Interfaces;
using Application.ViewModels.CategoryViewModels;
using Applications;
using Applications.ViewModels.Response;
using AutoMapper;
using Domain.Entities;
using System.Net;


namespace Application.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public CategoryService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public async Task<CreateCategoryViewModel> CreateCategory(CreateCategoryViewModel categoryDTO)
        {
            var categoryOjb = _mapper.Map<Category>(categoryDTO);
            await _unitOfWork.CategoryRepository.AddAsync(categoryOjb);
            var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;
            if (isSuccess)
            {
                return _mapper.Map<CreateCategoryViewModel>(categoryOjb);
            }
            return null;
        }
        public async Task<Pagination<CategoryViewModel>> GetAllCategories(int pageIndex = 0, int pageSize = 10)
        {
            var categories = await _unitOfWork.CategoryRepository.ToPagination(pageIndex, pageSize);
            var result = _mapper.Map<Pagination<CategoryViewModel>>(categories);
            var guidList = categories.Items.Select(x => x.CreatedBy).ToList();
            var users = await _unitOfWork.UserRepository.GetEntitiesByIdsAsync(guidList);

            foreach (var item in result.Items)
            {
                if (string.IsNullOrEmpty(item.CreatedBy)) continue;

                var createdBy = users.FirstOrDefault(x => x.Id == Guid.Parse(item.CreatedBy));
                if (createdBy != null)
                {
                    item.CreatedBy = createdBy.Email;
                }
            }
            return result;
        }
        public async Task<Response> GetCategoryById(Guid CategoryId)
        {
            var categories = await _unitOfWork.CategoryRepository.GetByIdAsync(CategoryId);
            var result = _mapper.Map<CategoryViewModel>(categories);
            var createBy = await _unitOfWork.UserRepository.GetByIdAsync(categories.CreatedBy);
            if (createBy != null)
            {
                result.CreatedBy = createBy.Email;
            }
            if (categories == null) return new Response(HttpStatusCode.NoContent, "Id not found");
            else return new Response(HttpStatusCode.OK, "Search succeed", result);
        }
        public async Task<Response> GetCategoryByName(string CategoryName, int pageIndex = 0, int pageSize = 10)
        {
            var categories = await _unitOfWork.CategoryRepository.GetCategoryByName(CategoryName, pageIndex, pageSize);
            if (categories.Items.Count() < 1) return new Response(HttpStatusCode.NoContent, "Not Found");
            else return new Response(HttpStatusCode.OK, "Search Succeed", _mapper.Map<Pagination<CategoryViewModel>>(categories));
        }
        public async Task<Response> GetCategoryByProductId(Guid ProductId, int pageIndex = 0, int pageSize = 10)
        {
            var categoryObj = await _unitOfWork.CategoryRepository.GetCategoryByProductId(ProductId, pageIndex, pageSize);
            if (categoryObj.Items.Count() < 1) return new Response(HttpStatusCode.NoContent, "Id not found");
            else return new Response(HttpStatusCode.OK, "Search Succeed", _mapper.Map<Pagination<CategoryViewModel>>(categoryObj));
        }
        public async Task<UpdateCategoryViewModel> UpdateCategory(Guid CategoryId, UpdateCategoryViewModel categoryDTO)
        {
            var categoryObj = await _unitOfWork.CategoryRepository.GetByIdAsync(CategoryId);
            if (categoryObj != null)
            {
                _mapper.Map(categoryDTO, categoryObj);
                _unitOfWork.CategoryRepository.Update(categoryObj);
                var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;
                if (isSuccess)
                {
                    return _mapper.Map<UpdateCategoryViewModel>(categoryObj);
                }
            }
            return null;
        }
    }
}
