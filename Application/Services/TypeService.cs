﻿using Application.Commos;
using Application.Interfaces;
using Application.ViewModels.TypeViewModels;
using Applications;
using Applications.ViewModels.Response;
using AutoMapper;
using System.Net;
using Type = Domain.Entities.Type;

namespace Application.Services
{
    public class TypeService : ITypeService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public TypeService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public async Task<CreateTypeViewModel> CreateType(CreateTypeViewModel typeDTO)
        {
            var typeObj = _mapper.Map<Type>(typeDTO);
            await _unitOfWork.TypeRepository.AddAsync(typeObj);
            var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;
            if (isSuccess)
            {
                return _mapper.Map<CreateTypeViewModel>(typeObj);
            }
            return null;
        }
        public async Task<Pagination<TypeViewModel>> GetAllTypes(int pageIndex = 0, int pageSize = 10)
        {
            var types = await _unitOfWork.TypeRepository.ToPagination(pageIndex, pageSize);
            var result = _mapper.Map<Pagination<TypeViewModel>>(types);
            var guidList = types.Items.Select(x => x.CreatedBy).ToList();
            var users = await _unitOfWork.UserRepository.GetEntitiesByIdsAsync(guidList);

            foreach (var item in result.Items)
            {
                if (string.IsNullOrEmpty(item.CreatedBy)) continue;

                var createdBy = users.FirstOrDefault(x => x.Id == Guid.Parse(item.CreatedBy));
                if (createdBy != null)
                {
                    item.CreatedBy = createdBy.Email;
                }
            }
            return result;
        }
        public async Task<Response> GetTypeByName(string TypeName, int pageIndex = 0, int pageSize = 10)
        {
            var types = await _unitOfWork.TypeRepository.GetTypeByName(TypeName, pageIndex, pageSize);
            if (types.Items.Count() < 1) return new Response(HttpStatusCode.NoContent, "Not Found");
            else return new Response(HttpStatusCode.OK, "Search Succeed", _mapper.Map<Pagination<TypeViewModel>>(types));
        }
        public async Task<UpdateTypeViewModel> UpdateType(Guid TypeId, UpdateTypeViewModel typeDTO)
        {
            var typeObj = await _unitOfWork.TypeRepository.GetByIdAsync(TypeId);
            if (typeObj != null)
            {
                _mapper.Map(typeDTO, typeObj);
                _unitOfWork.TypeRepository.Update(typeObj);
                var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;
                if (isSuccess)
                {
                    return _mapper.Map<UpdateTypeViewModel>(typeObj);
                }
            }
            return null;
        }
    }
}
