﻿using Application.Commos;
using Application.Interfaces;
using Application.ViewModels.ProductViewModels;
using Applications;
using Applications.ViewModels.Response;
using AutoMapper;
using Domain.Entities;
using System.Net;


namespace Application.Services
{
    public class ProductService : IProductService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public ProductService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public async Task<CreateProductViewModels> CreateProduct(CreateProductViewModels productDTO)
        {
            var productObj = _mapper.Map<Product>(productDTO);
            await _unitOfWork.ProductRepository.AddAsync(productObj);
            var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;
            if (isSuccess)
            {
                return _mapper.Map<CreateProductViewModels>(productObj);
            }
            return null;
        }
        public async Task<Pagination<ProductViewModels>> GetAllProducts(int pageIndex = 0, int pageSize = 10)
        {
            var products = await _unitOfWork.ProductRepository.ToPagination(pageIndex, pageSize);
            var result = _mapper.Map<Pagination<ProductViewModels>>(products);
            var guidList = products.Items.Select(x => x.CreatedBy).ToList();
            var users = await _unitOfWork.UserRepository.GetEntitiesByIdsAsync(guidList);

            foreach (var item in result.Items)
            {
                if (string.IsNullOrEmpty(item.CreatedBy)) continue;

                var createdBy = users.FirstOrDefault(x => x.Id == Guid.Parse(item.CreatedBy));
                if (createdBy != null)
                {
                    item.CreatedBy = createdBy.Email;
                }
            }
            return result;
        }
        public async Task<Response> GetProductByName(string ProductName, int pageIndex = 0, int pageSize = 10)
        {
            var products = await _unitOfWork.ProductRepository.GetProductByName(ProductName, pageIndex, pageSize);
            if (products.Items.Count() < 1) return new Response(HttpStatusCode.NoContent, "Not Found");
            else return new Response(HttpStatusCode.OK, "Search Succeed", _mapper.Map<Pagination<ProductViewModels>>(products));
        }
        public async Task<Response> GetProductById(Guid ProductId)
        {
            var products = await _unitOfWork.ProductRepository.GetByIdAsync(ProductId);
            var result = _mapper.Map<ProductViewModels>(products);
            var createBy = await _unitOfWork.UserRepository.GetByIdAsync(products.CreatedBy);
            if (createBy != null)
            {
                result.CreatedBy = createBy.Email;
            }
            if (products == null) return new Response(HttpStatusCode.NoContent, "Id not found");
            else return new Response(HttpStatusCode.OK, "Search succeed", result);
        }
        public async Task<Response> GetProductByBrandId(Guid BrandId, int pageIndex = 0, int pageSize = 10)
        {
            var productObj = await _unitOfWork.ProductRepository.GetProductByBrandId(BrandId, pageIndex, pageSize);
            if (productObj.Items.Count() < 1) return new Response(HttpStatusCode.NoContent, "Id not found");
            else return new Response(HttpStatusCode.OK, "Search Succeed", _mapper.Map<Pagination<ProductViewModels>>(productObj));
        }
        public async Task<UpdateProductViewModels> UpdateProduct(Guid ProductId, UpdateProductViewModels productDTO)
        {
            var productObj = await _unitOfWork.ProductRepository.GetByIdAsync(ProductId);
            if (productObj != null)
            {
                _mapper.Map(productDTO, productObj);
                _unitOfWork.ProductRepository.Update(productObj);
                var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;
                if (isSuccess)
                {
                    return _mapper.Map<UpdateProductViewModels>(productObj);
                }
            } 
            return null;
        }
    }
}
