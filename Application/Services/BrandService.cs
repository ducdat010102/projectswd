﻿using Application.Commos;
using Application.Interfaces;
using Application.ViewModels.BrandViewModels;
using Applications;
using Applications.ViewModels.Response;
using AutoMapper;
using Domain.Entities;

using System.Net;


namespace Application.Services
{
    public class BrandService : IBrandService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public BrandService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public async Task<CreateBrandViewModels> CreateBrand(CreateBrandViewModels brandDTO)
        {
            var brandOjb = _mapper.Map<Brand>(brandDTO);
            await _unitOfWork.BrandRepository.AddAsync(brandOjb);
            var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;
            if (isSuccess)
            {
                return _mapper.Map<CreateBrandViewModels>(brandOjb);
            }
            return null;
        }
        public async Task<Pagination<BrandViewModels>> GetAllBrands(int pageIndex = 0, int pageSize = 10)
        {
            var brands = await _unitOfWork.BrandRepository.ToPagination(pageIndex, pageSize);
            var result = _mapper.Map<Pagination<BrandViewModels>>(brands);
            var guidList = brands.Items.Select(x => x.CreatedBy).ToList();
            var users = await _unitOfWork.UserRepository.GetEntitiesByIdsAsync(guidList);

            foreach (var item in result.Items)
            {
                if (string.IsNullOrEmpty(item.CreatedBy)) continue;

                var createdBy = users.FirstOrDefault(x => x.Id == Guid.Parse(item.CreatedBy));
                if (createdBy != null)
                {
                    item.CreatedBy = createdBy.Email;
                }
            }
            return result;
        }
        public async Task<Response> GetBrandById(Guid BrandId)
        {
            var brands = await _unitOfWork.BrandRepository.GetByIdAsync(BrandId);
            var result = _mapper.Map<BrandViewModels>(brands);
            var createBy = await _unitOfWork.UserRepository.GetByIdAsync(brands.CreatedBy);
            if (createBy != null)
            {
                result.CreatedBy = createBy.Email;
            }
            if (brands == null) return new Response(HttpStatusCode.NoContent, "Id not found");
            else return new Response(HttpStatusCode.OK, "Search succeed", result);
        }
        public async Task<Response> GetBrandByName(string BrandName, int pageIndex = 0, int pageSize = 10)
        {
            var brands = await _unitOfWork.BrandRepository.GetBrandByName(BrandName, pageIndex, pageSize);
            if (brands.Items.Count() < 1) return new Response(HttpStatusCode.NoContent, "Not Found");
            else return new Response(HttpStatusCode.OK, "Search Succeed", _mapper.Map<Pagination<BrandViewModels>>(brands));
        }
        public async Task<UpdateBrandViewModels> UpdateBrand(Guid BrandId, UpdateBrandViewModels brandDTO)
        {
            var brandObj = await _unitOfWork.BrandRepository.GetByIdAsync(BrandId);
            if (brandObj != null)
            {
                _mapper.Map(brandDTO, brandObj);
                _unitOfWork.BrandRepository.Update(brandObj);
                var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;
                if (isSuccess)
                {
                    return _mapper.Map<UpdateBrandViewModels>(brandObj);
                }
            }
            return null;
        }
    }
}
