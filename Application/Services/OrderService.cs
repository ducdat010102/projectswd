﻿using Application.Commos;
using Application.Interfaces;
using Application.ViewModels.OrderViewModels;
using Application.ViewModels.ProductViewModels;
using Applications;
using Applications.ViewModels.Response;
using AutoMapper;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class OrderService : IOrderService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public OrderService(IUnitOfWork unitOfWork, IMapper mapper) 
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<CreateOrderViewModel> CreateOrder(CreateOrderViewModel orderDTO)
        {
            var order = _mapper.Map<Order>(orderDTO);
            await _unitOfWork.OrderRepository.AddAsync(order);
            var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;
            if (isSuccess)
            {
                return _mapper.Map<CreateOrderViewModel>(order);
               
            }
            var lated = _mapper.Map<CreateOrderViewModel>(order);
            return null;
        }
        public async Task<UpdateOrderViewModel> UpdateOrder(Guid OrderId, UpdateOrderViewModel orderDTO)
        {
            var productObj = await _unitOfWork.OrderRepository.GetByIdAsync(OrderId);
            if (productObj != null)
            {
                _mapper.Map(orderDTO, productObj);
                _unitOfWork.OrderRepository.Update(productObj);
                var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;
                if (isSuccess)
                {
                    return _mapper.Map<UpdateOrderViewModel>(productObj);
                }
            }
            return null;
        }
        public async Task<Pagination<OrderViewModel>> GetAllOrders(int pageIndex = 0, int pageSize = 10)
        {
            var orders = await _unitOfWork.OrderRepository.ToPagination(pageIndex, pageSize);
            var result = _mapper.Map<Pagination<OrderViewModel>>(orders);
            var guidList = orders.Items.Select(x => x.CreatedBy).ToList();
            var users = await _unitOfWork.UserRepository.GetEntitiesByIdsAsync(guidList);

            foreach (var item in result.Items)
            {
                if (string.IsNullOrEmpty(item.CreatedBy)) continue;

                var createdBy = users.FirstOrDefault(x => x.Id == Guid.Parse(item.CreatedBy));
                if (createdBy != null)
                {
                    item.CreatedBy = createdBy.Email;
                }
            }
            return result;
        }
        
        public async Task<Response> GetOrderByUserId(Guid UserId, int pageIndex = 0, int pageSize = 10)
        {
            var productObj = await _unitOfWork.OrderRepository.GetOrderByUserId(UserId, pageIndex, pageSize);
            if (productObj.Items.Count() < 1) return new Response(HttpStatusCode.NoContent, "Id not found");
            else return new Response(HttpStatusCode.OK, "Search Succeed", _mapper.Map<Pagination<OrderViewModel>>(productObj));
        }
    }
}
