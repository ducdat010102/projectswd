﻿using Application.Commos;
using Application.Interfaces;
using Application.ViewModels.ServiceViewModels;
using Applications;
using Applications.ViewModels.Response;
using AutoMapper;
using Domain.Entities;
using System.Net;

namespace Application.Services
{
    public class ServiceService : IServiceService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public ServiceService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public async Task<CreateServiceViewModel> CreateService(CreateServiceViewModel serviceDTO)
        {
            var serviceObj = _mapper.Map<Service>(serviceDTO);
            await _unitOfWork.ServiceRepository.AddAsync(serviceObj);
            var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;
            if (isSuccess)
            {
                return _mapper.Map<CreateServiceViewModel>(serviceObj);
            }
            return null;
        }
        public async Task<Pagination<ServiceViewModel>> GetAllServices(int pageIndex = 0, int pageSize = 10)
        {
            var services = await _unitOfWork.ServiceRepository.ToPagination(pageIndex, pageSize);
            var result = _mapper.Map<Pagination<ServiceViewModel>>(services);
            var guidList = services.Items.Select(x => x.CreatedBy).ToList();
            var users = await _unitOfWork.UserRepository.GetEntitiesByIdsAsync(guidList);

            foreach (var item in result.Items)
            {
                if (string.IsNullOrEmpty(item.CreatedBy)) continue;

                var createdBy = users.FirstOrDefault(x => x.Id == Guid.Parse(item.CreatedBy));
                if (createdBy != null)
                {
                    item.CreatedBy = createdBy.Email;
                }
            }
            return result;
        }
        public async Task<Response> GetServiceByName(string ServiceName, int pageIndex = 0, int pageSize = 10)
        {
            var services = await _unitOfWork.ServiceRepository.GetServiceByName(ServiceName, pageIndex, pageSize);
            if (services.Items.Count() < 1) return new Response(HttpStatusCode.NoContent, "Not Found");
            else return new Response(HttpStatusCode.OK, "Search Succeed", _mapper.Map<Pagination<ServiceViewModel>>(services));
        }

        public async Task<Response> GetServiceById(Guid ServiceId)
        {
            var services = await _unitOfWork.ServiceRepository.GetByIdAsync(ServiceId);
            var result = _mapper.Map<ServiceViewModel>(services);
            var createBy = await _unitOfWork.UserRepository.GetByIdAsync(services.CreatedBy);
            if (createBy != null)
            {
                result.CreatedBy = createBy.Email;
            }
            if (services == null) return new Response(HttpStatusCode.NoContent, "Id not found");
            else return new Response(HttpStatusCode.OK, "Search succeed", result);
        }

        public async Task<Response> GetServiceByOrderDetailId(Guid OrderDetailId, int pageIndex = 0, int pageSize = 10)
        {
            var serviceObj = await _unitOfWork.ServiceRepository.GetServiceByOrderDetailId(OrderDetailId, pageIndex, pageSize);
            if (serviceObj.Items.Count() < 1) return new Response(HttpStatusCode.NoContent, "Id not found");
            else return new Response(HttpStatusCode.OK, "Search Succeed", _mapper.Map<Pagination<ServiceViewModel>>(serviceObj));
        }
        public async Task<UpdateServiceViewModel> UpdateService(Guid ServiceId, UpdateServiceViewModel serviceDTO)
        {
            var serviceObj = await _unitOfWork.ServiceRepository.GetByIdAsync(ServiceId);
            if (serviceObj != null)
            {
                _mapper.Map(serviceDTO, serviceObj);
                _unitOfWork.ServiceRepository.Update(serviceObj);
                var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;
                if (isSuccess)
                {
                    return _mapper.Map<UpdateServiceViewModel>(serviceObj);
                }
            }
            return null;
        }
    }
}
