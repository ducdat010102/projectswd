﻿using Application.Commos;
using Application.Interfaces;
using Application.ViewModels.OrderDetailViewModels;
using Applications;
using Applications.ViewModels.Response;
using AutoMapper;
using Domain.Entities;
using System.Net;

namespace Application.Services
{
    public class OrderDetailService : IOrderDetailService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public OrderDetailService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public async Task<CreateOrderDetailViewModel> CreateOrderDetail(CreateOrderDetailViewModel orderdetailDTO)
        {
            var orderdetailOjb = _mapper.Map<OrderDetail>(orderdetailDTO);
            await _unitOfWork.OrderDetailRepository.AddAsync(orderdetailOjb);
            var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;
            if (isSuccess)
            {
                return _mapper.Map<CreateOrderDetailViewModel>(orderdetailOjb);
            }
            return null;
        }
        public async Task<Pagination<OrderDetailViewModel>> GetAllOrderDetails(int pageIndex = 0, int pageSize = 10)
        {
            var orderdetails = await _unitOfWork.OrderDetailRepository.ToPagination(pageIndex, pageSize);
            var result = _mapper.Map<Pagination<OrderDetailViewModel>>(orderdetails);
            var guidList = orderdetails.Items.Select(x => x.CreatedBy).ToList();
            var users = await _unitOfWork.UserRepository.GetEntitiesByIdsAsync(guidList);

            foreach (var item in result.Items)
            {
                if (string.IsNullOrEmpty(item.CreatedBy)) continue;

                var createdBy = users.FirstOrDefault(x => x.Id == Guid.Parse(item.CreatedBy));
                if (createdBy != null)
                {
                    item.CreatedBy = createdBy.Email;
                }
            }
            return result;
        }
        public async Task<Response> GetOrderDetailById(Guid OrderDetailId)
        {
            var orderdetails = await _unitOfWork.OrderDetailRepository.GetByIdAsync(OrderDetailId);
            var result = _mapper.Map<OrderDetailViewModel>(orderdetails);
            var createBy = await _unitOfWork.UserRepository.GetByIdAsync(orderdetails.CreatedBy);
            if (createBy != null)
            {
                result.CreatedBy = createBy.Email;
            }
            if (orderdetails == null) return new Response(HttpStatusCode.NoContent, "Id not found");
            else return new Response(HttpStatusCode.OK, "Search succeed", result);
        }
        public async Task<Response> GetOrderDetailByProductId(Guid ProductId, int pageIndex = 0, int pageSize = 10)
        {
            var orderdetailObj = await _unitOfWork.OrderDetailRepository.GetOrderDetailByProductId(ProductId, pageIndex, pageSize);
            if (orderdetailObj.Items.Count() < 1) return new Response(HttpStatusCode.NoContent, "Id not found");
            else return new Response(HttpStatusCode.OK, "Search Succeed", _mapper.Map<Pagination<OrderDetailViewModel>>(orderdetailObj));
        }
        public async Task<Response> GetOrderDetailByOrderId(Guid OrderId, int pageIndex = 0, int pageSize = 10)
        {
            var orderdetailObj = await _unitOfWork.OrderDetailRepository.GetOrderDetailByOrderId(OrderId, pageIndex, pageSize);
            if (orderdetailObj.Items.Count() < 1) return new Response(HttpStatusCode.NoContent, "Id not found");
            else return new Response(HttpStatusCode.OK, "Search Succeed", _mapper.Map<Pagination<OrderDetailViewModel>>(orderdetailObj));
        }
        public async Task<UpdateOrderDetailViewModel> UpdateOrderDetail(Guid OrderDetailId, UpdateOrderDetailViewModel orderdetailDTO)
        {
            var orderdetailObj = await _unitOfWork.OrderDetailRepository.GetByIdAsync(OrderDetailId);
            if (orderdetailObj != null)
            {
                _mapper.Map(orderdetailDTO, orderdetailObj);
                _unitOfWork.OrderDetailRepository.Update(orderdetailObj);
                var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;
                if (isSuccess)
                {
                    return _mapper.Map<UpdateOrderDetailViewModel>(orderdetailObj);
                }
            }
            return null;
        }
    }
}
