﻿using Application.Commos;
using Application.ViewModels.OrderViewModels;
using Application.ViewModels.ProductViewModels;
using Applications.ViewModels.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IOrderService
    {
        public Task<CreateOrderViewModel> CreateOrder(CreateOrderViewModel orderDTO);
        public Task<UpdateOrderViewModel> UpdateOrder(Guid OrderId, UpdateOrderViewModel productDTO);
        public Task<Response> GetOrderByUserId(Guid UserId, int pageIndex = 0, int pageSize = 10);
        public Task<Pagination<OrderViewModel>> GetAllOrders(int pageIndex = 0, int pageSize = 10);
    }
}
