﻿using Application.Commos;
using Application.ViewModels.BrandViewModels;
using Applications.ViewModels.Response;

namespace Application.Interfaces
{
    public interface IBrandService
    {
        
        public Task<Pagination<BrandViewModels>> GetAllBrands(int pageIndex = 0, int pageSize = 10);
        public Task<Response> GetBrandById(Guid BrandId);
        public Task<Response> GetBrandByName(string BrandName, int pageIndex = 0, int pageSize = 10);
        public Task<CreateBrandViewModels> CreateBrand(CreateBrandViewModels brandDTO);
        public Task<UpdateBrandViewModels> UpdateBrand(Guid BrandId, UpdateBrandViewModels brandDTO);
        


    }
}
