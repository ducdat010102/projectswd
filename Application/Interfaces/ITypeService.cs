﻿using Application.Commos;
using Application.ViewModels.TypeViewModels;
using Applications.ViewModels.Response;

namespace Application.Interfaces
{
    public interface ITypeService
    {
        public Task<CreateTypeViewModel> CreateType(CreateTypeViewModel TypeDTO);
        public Task<Pagination<TypeViewModel>> GetAllTypes(int pageIndex = 0, int pageSize = 10);
        public Task<Response> GetTypeByName(string TypeNameName, int pageIndex = 0, int pageSize = 10);

        public Task<UpdateTypeViewModel> UpdateType(Guid TypeId, UpdateTypeViewModel typeDTO);

    }
}
