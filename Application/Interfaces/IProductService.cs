﻿using Application.Commos;
using Application.ViewModels.BrandViewModels;
using Application.ViewModels.ProductViewModels;
using Applications.ViewModels.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IProductService
    {
        public Task<CreateProductViewModels> CreateProduct(CreateProductViewModels productDTO);
        public Task<Pagination<ProductViewModels>> GetAllProducts(int pageIndex = 0, int pageSize = 10);
        public Task<Response> GetProductByName(string ProductName, int pageIndex = 0, int pageSize = 10);
        public Task<Response> GetProductById(Guid ProductId);
        public Task<Response> GetProductByBrandId(Guid BrandId, int pageIndex = 0, int pageSize = 10);
        public Task<UpdateProductViewModels> UpdateProduct(Guid ProductId, UpdateProductViewModels productDTO);

    }
}
