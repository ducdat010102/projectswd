﻿using Application.Commos;
using Application.ViewModels.ServiceViewModels;
using Applications.ViewModels.Response;

namespace Application.Interfaces
{
    public interface IServiceService
    {
        public Task<CreateServiceViewModel> CreateService(CreateServiceViewModel ServiceDTO);
        public Task<Pagination<ServiceViewModel>> GetAllServices(int pageIndex = 0, int pageSize = 10);
        public Task<Response> GetServiceByName(string ServiceName, int pageIndex = 0, int pageSize = 10);
        public Task<Response> GetServiceById(Guid ServiceId);
        public Task<Response> GetServiceByOrderDetailId(Guid OrderDetailId, int pageIndex = 0, int pageSize = 10);
        public Task<UpdateServiceViewModel> UpdateService(Guid ServiceId, UpdateServiceViewModel serviceDTO);
    }
}
