﻿using Application.Commos;
using Application.ViewModels.OrderDetailViewModels;
using Applications.ViewModels.Response;

namespace Application.Interfaces
{
    public interface IOrderDetailService
    {
        public Task<CreateOrderDetailViewModel> CreateOrderDetail(CreateOrderDetailViewModel orderDTO);
        public Task<Response> GetOrderDetailById(Guid OrderDetailId);
        public Task<Pagination<OrderDetailViewModel>> GetAllOrderDetails(int pageIndex = 0, int pageSize = 10);
        public Task<Response> GetOrderDetailByProductId(Guid ProductId, int pageIndex = 0, int pageSize = 10);
        public Task<Response> GetOrderDetailByOrderId(Guid OrderId, int pageIndex = 0, int pageSize = 10);
        public Task<UpdateOrderDetailViewModel> UpdateOrderDetail(Guid OrderDetailId, UpdateOrderDetailViewModel categoryDTO);

    }
}
