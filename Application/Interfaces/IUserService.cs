﻿using Application.Commos;
using Application.Repositories;
using Application.ViewModels.UserViewModels;
using Applications.ViewModels.Response;
using Domain.Entities;


namespace Application.Interfaces
{
    public interface IUserService 
    {
        Task<Pagination<UserViewModel>> GetAllUsers(int pageIndex = 0, int pageSize = 10);
        //Task<Response> Login(UserLoginViewModel userLoginViewModel);
        Task<Response> AddUser(CreateUserViewModel createUserViewModel);

        public bool GetLoginUser(UserLoginViewModel userLoginViewModel);


    }
}
