﻿
using Application.Commos;
using Application.ViewModels.CategoryViewModels;
using Applications.ViewModels.Response;

namespace Application.Interfaces
{
    public interface ICategoryService
    {
        public Task<CreateCategoryViewModel> CreateCategory(CreateCategoryViewModel categoryDTO);
        public Task<Pagination<CategoryViewModel>> GetAllCategories(int pageIndex = 0, int pageSize = 10);
        public Task<Response> GetCategoryById(Guid CategoryId);
        public Task<Response> GetCategoryByName(string CategoryName, int pageIndex = 0, int pageSize = 10);
        public Task<Response> GetCategoryByProductId(Guid ProductId, int pageIndex = 0, int pageSize = 10);
        public Task<UpdateCategoryViewModel> UpdateCategory(Guid CategoryId, UpdateCategoryViewModel categoryDTO);
    }
}
