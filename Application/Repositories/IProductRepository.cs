﻿using Application.Commos;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Repositories
{
    public interface IProductRepository : IGenericRepository<Product>
    {
        Task<Pagination<Product>> GetProductByName(string ProductName, int pageNumber = 0, int pageSize = 10);
        Task<Pagination<Product>> GetProductByBrandId(Guid BrandId, int pageIndex = 0, int pageSize = 10);

    }
}
