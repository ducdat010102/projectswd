﻿using Application.Commos;
using Applications.ViewModels.Response;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Repositories
{
    public interface IOrderRepository : IGenericRepository<Order>
    {
        public Task<Pagination<Order>> GetOrderByUserId(Guid UserId, int pageNumber = 0, int pageSize = 10);
        
    }
}
