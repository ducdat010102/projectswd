﻿using Application.Commos;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Repositories
{
    public interface IBrandRepository : IGenericRepository<Brand>
    {
        Task<Pagination<Brand>> GetBrandByName(string BrandName, int pageNumber = 0, int pageSize = 10);

    }
}
