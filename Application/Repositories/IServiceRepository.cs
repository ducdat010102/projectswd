﻿using Application.Commos;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Repositories
{
    public interface IServiceRepository : IGenericRepository<Service>
    {
        Task<Pagination<Service>> GetServiceByName(string ServiceName, int pageNumber = 0, int pageSize = 10);
        Task<Pagination<Service>> GetServiceByOrderDetailId(Guid OrderDetailId, int pageIndex = 0, int pageSize = 10);
    }
}
