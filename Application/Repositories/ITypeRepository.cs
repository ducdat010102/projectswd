﻿using Application.Commos;
using Type = Domain.Entities.Type;

namespace Application.Repositories
{
    public interface ITypeRepository : IGenericRepository<Type>
    {
        Task<Pagination<Type>> GetTypeByName(string TypeName, int pageNumber = 0, int pageSize = 10);


    }
}
