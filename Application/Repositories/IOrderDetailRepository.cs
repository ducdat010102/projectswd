﻿using Application.Commos;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Repositories
{
    public interface IOrderDetailRepository : IGenericRepository<OrderDetail>
    {
        Task<Pagination<OrderDetail>> GetOrderDetailByProductId(Guid ProductId, int pageIndex = 0, int pageSize = 10);
        Task<Pagination<OrderDetail>> GetOrderDetailByOrderId(Guid OrderId, int pageIndex = 0, int pageSize = 10);

    }
}
