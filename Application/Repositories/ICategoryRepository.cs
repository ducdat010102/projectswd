﻿using Application.Commos;
using Domain.Entities;

namespace Application.Repositories
{
    public interface ICategoryRepository : IGenericRepository<Category>
    {
        Task<Pagination<Category>> GetCategoryByName(string CategoryName, int pageNumber = 0, int pageSize = 10);
        Task<Pagination<Category>> GetCategoryByProductId(Guid ProductId, int pageIndex = 0, int pageSize = 10);

    }
}
