﻿using Application.Commos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Repositories
{
    public interface IGenericRepository<TEntity>
    {
        Task<List<TEntity>> GetEntitiesByIdsAsync(List<Guid?> Ids);
        Task<List<TEntity>> GetAllAsync();
        Task<TEntity?> GetByIdAsync(Guid? id);
        Task AddAsync(TEntity entity);
        void Update(TEntity entity);
        void Approve(TEntity entity);
        Task<Pagination<TEntity>> ToPagination(int pageNumber = 0, int pageSize = 10);
    }
}
