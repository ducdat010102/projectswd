﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.CategoryViewModels
{
    public class CreateCategoryViewModel
    {
        public string CategoryName { get; set; }
        public string? Description { get; set; }
        public Guid ProductId { get; set; }
    }
}
