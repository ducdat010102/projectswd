﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.ServiceViewModels
{
    public class UpdateServiceViewModel
    {
        public string ServiceName { get; set; }
        public float Price { get; set; }
        public string? Description { get; set; }
    }
}
