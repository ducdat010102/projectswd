﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.TypeViewModels
{
    public class UpdateTypeViewModel
    {
        public string TypeName { get; set; }
        public string? Description { get; set; }
    }
}
