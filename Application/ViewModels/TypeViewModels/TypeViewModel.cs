﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.TypeViewModels
{
    public class TypeViewModel
    {
        public Guid Id { get; set; }
        public string TypeName { get; set; }
        public string? Description { get; set; }
        public DateTime CreationDate { get; set; }
        public string? CreatedBy { get; set; }
        public DateTime? DeletionDate { get; set; }
        public Guid? DeleteBy { get; set; }
    }
}
