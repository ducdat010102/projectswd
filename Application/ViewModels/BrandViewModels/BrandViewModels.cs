﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.BrandViewModels
{
    public class BrandViewModels
    {
        public Guid Id { get; set; }
        public string BrandName { get; set; }
        public string Address { get; set; }
        public string Hotline { get; set; }
        public string? Description { get; set; }
        public DateTime CreationDate { get; set; }
        public string? CreatedBy { get; set; }
        public DateTime? DeletionDate { get; set; }
        public Guid? DeleteBy { get; set; }
    }
}
