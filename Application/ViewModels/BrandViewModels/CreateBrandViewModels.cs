﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.BrandViewModels
{
    public class CreateBrandViewModels
    {
        public string BrandName { get; set; }
        public string Address { get; set; }
        public string Hotline { get; set; }
        public string? Description { get; set; }
        
    }
}
