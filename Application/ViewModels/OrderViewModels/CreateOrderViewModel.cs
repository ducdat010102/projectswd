﻿using Domain.Entities;
using Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.OrderViewModels
{
    public class CreateOrderViewModel
    {
        public DateTime OrderDate { get; set; }
        public float Total { get; set; }
        public float Discount { get; set; }
        public Status Status { get; set; }
        public Guid UserId { get; set; }
    }
}
