﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.OrderDetailViewModels
{
    public class CreateOrderDetailViewModel
    {
        public float Subtotal { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public Guid OrderId { get; set; }
        public Guid ProductId { get; set; }
    }
}
