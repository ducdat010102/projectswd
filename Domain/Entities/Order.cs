﻿using Domain.Base;
using Domain.Enum;

namespace Domain.Entities
{
    public class Order : BaseEntity
    {
        public DateTime OrderDate { get; set; }
        public float Total { get; set; }
        public float Discount { get; set; }
        public Status Status { get; set; }
        public ICollection<OrderDetail> OrderDetails { get; set; }
        public Guid UserId { get; set; }
        public User User { get; set; }

    }
}
