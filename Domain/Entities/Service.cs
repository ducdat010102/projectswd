﻿using Domain.Base;

namespace Domain.Entities
{
    public class Service : BaseEntity
    {
        public string ServiceName { get; set; }
        public float Price { get; set; }
        public string? Description { get; set; }
        public Guid OrderDetailId { get; set; }
        public OrderDetail OrderDetail { get; set; }
    }
}
