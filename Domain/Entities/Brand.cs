﻿using Domain.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class Brand : BaseEntity
    {
        public string BrandName { get; set; }
        public string Address { get; set; }
        public string Hotline { get; set; }
        public string? Description { get; set; }
        public ICollection<Product> Products { get; set; }
    }
}
