﻿using Domain.Base;
using Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class Product : BaseEntity
    {
        public string ProductName { get; set; }
        public float Price { get; set; }
        public string? Description { get; set; }
        public Status Status { get; set; }
        public Guid BrandId { get; set; }
        public Brand Brand { get; set; }
        public ICollection<Category> Categories { get; set; }
        public ICollection<Type> Types { get; set; }
        public ICollection<OrderDetail> OrderDetails { get; set; }

    }
}
