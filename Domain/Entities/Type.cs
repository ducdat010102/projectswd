﻿using Domain.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class Type : BaseEntity
    {
        public string TypeName { get; set; }
        public string? Description { get; set; }
        public Guid ProductId { get; set; }
        public Product Product { get; set; }
    }
}
