﻿using Application.ViewModels.BrandViewModels;
using Application.ViewModels.CategoryViewModels;
using Application.ViewModels.OrderDetailViewModels;
using Application.ViewModels.OrderViewModels;
using Application.ViewModels.ProductViewModels;
using Application.ViewModels.ServiceViewModels;
using Application.ViewModels.TypeViewModels;
using Applications;
using FluentValidation;
using Infrastructures;
using Project_SWD.Validations.BrandValidation;
using Project_SWD.Validations.CategoryValidation;
using Project_SWD.Validations.OrderDetailValidation;
using Project_SWD.Validations.OrderValidation;
using Project_SWD.Validations.ProductValidation;
using Project_SWD.Validations.ServiceValidation;
using Project_SWD.Validations.TypeValidation;
using System.Text.Json.Serialization;

namespace Project_SWD
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddWebAPIService(this IServiceCollection services)
        {
            services.AddControllers().AddJsonOptions(opt =>
            opt.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles);
            services.AddEndpointsApiExplorer();
            services.AddSwaggerGen();
            services.AddHealthChecks();
            services.AddHttpContextAccessor();
            services.AddScoped<IValidator<CreateBrandViewModels>, CreateBrandValidation>();
            services.AddScoped<IValidator<UpdateBrandViewModels>, UpdateBrandValidation>();

            services.AddScoped<IValidator<CreateProductViewModels>, CreateProductValidation>();
            services.AddScoped<IValidator<UpdateProductViewModels>, UpdateProductValidation>();

            services.AddScoped<IValidator<CreateCategoryViewModel>, CreateCategoryValidation>();
            services.AddScoped<IValidator<UpdateCategoryViewModel>, UpdateCategoryValidation>();

            services.AddScoped<IValidator<CreateTypeViewModel>, CreateTypeValidation>();
            services.AddScoped<IValidator<UpdateTypeViewModel>, UpdateTypeValidation>();

            services.AddScoped<IValidator<CreateOrderViewModel>, CreateOrderValidation>();

            services.AddScoped<IValidator<CreateServiceViewModel>, CreateServiceValidation>();
            services.AddScoped<IValidator<UpdateServiceViewModel>, UpdateServiceValidation>();

            services.AddScoped<IValidator<CreateOrderDetailViewModel>, CreateOrderDetailValidation>();
            services.AddScoped<IValidator<UpdateOrderDetailViewModel>, UpdateOrderDetailValidation>();
            return services;
        }
    }
}
