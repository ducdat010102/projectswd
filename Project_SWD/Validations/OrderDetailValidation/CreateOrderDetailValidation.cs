﻿using Application.ViewModels.OrderDetailViewModels;
using FluentValidation;

namespace Project_SWD.Validations.OrderDetailValidation
{
    public class CreateOrderDetailValidation : AbstractValidator<CreateOrderDetailViewModel>
    {
        public CreateOrderDetailValidation()
        {
            RuleFor(x => x.BeginDate).NotEmpty().NotNull();

        }
    }
}
