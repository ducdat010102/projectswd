﻿using Application.ViewModels.OrderDetailViewModels;
using FluentValidation;

namespace Project_SWD.Validations.OrderDetailValidation
{
    public class UpdateOrderDetailValidation : AbstractValidator<UpdateOrderDetailViewModel>
    {
        public UpdateOrderDetailValidation()
        {
            RuleFor(x => x.BeginDate).NotNull().NotEmpty();
        }
    }
}
