﻿using Application.ViewModels.OrderViewModels;
using Application.ViewModels.ProductViewModels;
using FluentValidation;

namespace Project_SWD.Validations.OrderValidation
{
    public class CreateOrderValidation : AbstractValidator<CreateOrderViewModel>
    {
        public CreateOrderValidation()
        {
            RuleFor(x => x.OrderDate).NotEmpty().NotNull();
        }

    }
}
