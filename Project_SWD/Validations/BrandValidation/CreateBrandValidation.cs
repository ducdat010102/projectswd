﻿using Application.ViewModels.BrandViewModels;

using FluentValidation;

namespace Project_SWD.Validations.BrandValidation
{
    public class CreateBrandValidation : AbstractValidator<CreateBrandViewModels>
    {
        public CreateBrandValidation()
        {
            RuleFor(x => x.BrandName).NotEmpty().NotNull().MaximumLength(100);

        }
    }
}
