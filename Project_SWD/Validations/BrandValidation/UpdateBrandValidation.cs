﻿using Application.ViewModels.BrandViewModels;
using FluentValidation;

namespace Project_SWD.Validations.BrandValidation
{
    public class UpdateBrandValidation : AbstractValidator<UpdateBrandViewModels>
    {
        public UpdateBrandValidation()
        {
            RuleFor(x => x.BrandName).NotNull().NotEmpty().MaximumLength(100);
            RuleFor(x => x.Description).NotNull().NotEmpty().MaximumLength(100);
            
        }
    }
}
