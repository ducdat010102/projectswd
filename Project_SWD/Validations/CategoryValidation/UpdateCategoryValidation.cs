﻿
using Application.ViewModels.CategoryViewModels;
using FluentValidation;

namespace Project_SWD.Validations.CategoryValidation
{
    public class UpdateCategoryValidation : AbstractValidator<UpdateCategoryViewModel>
    {
        public UpdateCategoryValidation()
        {
            RuleFor(x => x.CategoryName).NotNull().NotEmpty().MaximumLength(100);
            RuleFor(x => x.Description).NotNull().NotEmpty().MaximumLength(100);

        }
    }
}
