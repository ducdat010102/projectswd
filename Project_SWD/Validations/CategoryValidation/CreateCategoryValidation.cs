﻿using Application.ViewModels.CategoryViewModels;
using FluentValidation;

namespace Project_SWD.Validations.CategoryValidation
{
    public class CreateCategoryValidation : AbstractValidator<CreateCategoryViewModel>
    {
        public CreateCategoryValidation()
        {
            RuleFor(x => x.CategoryName).NotEmpty().NotNull().MaximumLength(100);

        }
    }
}
