﻿using Application.ViewModels.ServiceViewModels;
using FluentValidation;

namespace Project_SWD.Validations.ServiceValidation
{
    public class UpdateServiceValidation : AbstractValidator<UpdateServiceViewModel>
    {
        public UpdateServiceValidation()
        {
            RuleFor(x => x.ServiceName).NotNull().NotEmpty().MaximumLength(100);

        }
    }
}
