﻿using Application.ViewModels.ProductViewModels;
using Application.ViewModels.ServiceViewModels;
using FluentValidation;

namespace Project_SWD.Validations.ServiceValidation
{
    public class CreateServiceValidation : AbstractValidator<CreateServiceViewModel>
    {
        public CreateServiceValidation()
        {
            RuleFor(x => x.ServiceName).NotEmpty().NotNull().MaximumLength(100);

        }
    }
}
