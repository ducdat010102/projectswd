﻿using Application.ViewModels.TypeViewModels;
using FluentValidation;

namespace Project_SWD.Validations.TypeValidation
{
    public class CreateTypeValidation : AbstractValidator<CreateTypeViewModel>
    {
        public CreateTypeValidation()
        {
            RuleFor(x => x.TypeName).NotEmpty().NotNull().MaximumLength(100);

        }

    }
}
