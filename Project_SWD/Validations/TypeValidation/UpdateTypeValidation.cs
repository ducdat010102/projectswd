﻿using Application.ViewModels.TypeViewModels;
using FluentValidation;

namespace Project_SWD.Validations.TypeValidation
{
    public class UpdateTypeValidation : AbstractValidator<UpdateTypeViewModel>
    {
        public UpdateTypeValidation()
        {
            RuleFor(x => x.TypeName).NotEmpty().NotNull().MaximumLength(100);

        }

    }
}
