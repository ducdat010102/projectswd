﻿using Application.ViewModels.ProductViewModels;
using FluentValidation;

namespace Project_SWD.Validations.ProductValidation
{
    public class CreateProductValidation : AbstractValidator<CreateProductViewModels>
    {
        public  CreateProductValidation()
        {
            RuleFor(x => x.ProductName).NotEmpty().NotNull().MaximumLength(100);

        }
            
    }
}
