﻿using Application.ViewModels.ProductViewModels;
using FluentValidation;

namespace Project_SWD.Validations.ProductValidation
{
    public class UpdateProductValidation : AbstractValidator<UpdateProductViewModels>
    {
        public UpdateProductValidation()
        {
            RuleFor(x => x.ProductName).NotNull().NotEmpty().MaximumLength(100);
            RuleFor(x => x.Price).NotNull().NotEmpty();

        }
    }
}
