﻿using Application.Commos;
using Application.Interfaces;
using Application.ViewModels.CategoryViewModels;
using Applications.ViewModels.Response;
using FluentValidation;
using FluentValidation.Results;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace Project_SWD.Controllers
{
    public class CategoryController : Controller
    {
        private readonly ICategoryService _categoryServices;
        private readonly IValidator<CreateCategoryViewModel> _validatorCreate;
        private readonly IValidator<UpdateCategoryViewModel> _validatorUpdate;
        public CategoryController(ICategoryService categoryServices,
            IValidator<CreateCategoryViewModel> validatorCreate,
            IValidator<UpdateCategoryViewModel> validatorUpdate
            )
        {
            _categoryServices = categoryServices;
            _validatorCreate = validatorCreate;
            _validatorUpdate = validatorUpdate;
        }
        [HttpPost("CreateCategory")]
        public async Task<Response> CreateCategory(CreateCategoryViewModel CategoryModel)
        {
            if (ModelState.IsValid)
            {
                ValidationResult validation = _validatorCreate.Validate(CategoryModel);
                if (validation.IsValid)
                {
                    await _categoryServices.CreateCategory(CategoryModel);
                    return new Response(HttpStatusCode.OK, "Create Category Succeed", CategoryModel);
                }
            }
            else
            {
                return new Response(HttpStatusCode.BadRequest, "Create Failed, Invalid input");
            }
            return new Response(HttpStatusCode.BadRequest, "Invalid Input");
        }
        [HttpGet("GetAllCategories")]
        public async Task<Pagination<CategoryViewModel>> GetAllCategories(int pageIndex = 0, int pageSize = 10) => await _categoryServices.GetAllCategories(pageIndex, pageSize);
        [HttpGet("GetCategoryById/{CategoryId}")]
        public async Task<Response> GetCategoryById(Guid CategoryId) => await _categoryServices.GetCategoryById(CategoryId);

        [HttpGet("GetCategoryByName/{CategoryName}")]
        public async Task<Response> GetCategoryByName(string CategoryName, int pageIndex = 0, int pageSize = 10)
        {
            return await _categoryServices.GetCategoryByName(CategoryName, pageIndex, pageSize);
        }
        [HttpGet("GetProductByProductId/{ProductId}")]
        public async Task<Response> GetCategoryByProductId(Guid ProductId, int pageIndex = 0, int pageSize = 10) => await _categoryServices.GetCategoryByProductId(ProductId, pageIndex, pageSize);
        [HttpPut("UpdateCategory/{CategoryId}")]
        public async Task<IActionResult> UpdateCategory(Guid CategoryId, UpdateCategoryViewModel Category)
        {
            if (ModelState.IsValid)
            {
                ValidationResult result = _validatorUpdate.Validate(Category);
                if (result.IsValid)
                {
                    if (await _categoryServices.UpdateCategory(CategoryId, Category) != null)
                    {
                        return Ok("Update Category Success");
                    }
                    return BadRequest("Invalid Id");
                }
            }
            return BadRequest("Update Failed,Invalid Input Information");
        }
    }
}
