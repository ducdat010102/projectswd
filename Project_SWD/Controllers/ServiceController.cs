﻿using Application.Commos;
using Application.Interfaces;
using Application.Services;
using Application.ViewModels.ProductViewModels;
using Application.ViewModels.ServiceViewModels;
using Applications.ViewModels.Response;
using FluentValidation;
using FluentValidation.Results;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace Project_SWD.Controllers
{
    public class ServiceController : Controller
    {
        private readonly IServiceService _serviceServices;
        private readonly IValidator<CreateServiceViewModel> _validatorCreate;
        private readonly IValidator<UpdateServiceViewModel> _validatorUpdate;
        public ServiceController(IServiceService serviceServices, IValidator<CreateServiceViewModel> validatorCreate, IValidator<UpdateServiceViewModel> validatorUpdate)
        {
            _serviceServices = serviceServices;
            _validatorCreate = validatorCreate;
            _validatorUpdate = validatorUpdate;
        }
        [HttpPost("CreateService")]
        public async Task<Response> CreateService(CreateServiceViewModel ServiceModel)
        {
            if (ModelState.IsValid)
            {
                ValidationResult validation = _validatorCreate.Validate(ServiceModel);
                if (validation.IsValid)
                {
                    await _serviceServices.CreateService(ServiceModel);
                    return new Response(HttpStatusCode.OK, "Create Service Succeed", ServiceModel);
                }
            }
            else
            {
                return new Response(HttpStatusCode.BadRequest, "Create Failed, Invalid input");
            }
            return new Response(HttpStatusCode.BadRequest, "Invalid Input");
        }
        [HttpGet("GetAllServices")]
        public async Task<Pagination<ServiceViewModel>> GetAllServices(int pageIndex = 0, int pageSize = 10) => await _serviceServices.GetAllServices(pageIndex, pageSize);


        [HttpGet("GetServiceByName/{ServiceName}")]
        public async Task<Response> GetServiceByName(string ServiceName, int pageIndex = 0, int pageSize = 10)
        {
            return await _serviceServices.GetServiceByName(ServiceName, pageIndex, pageSize);
        }

        [HttpGet("GetServiceById/{ServiceId}")]
        public async Task<Response> GetServiceById(Guid ServiceId) => await _serviceServices.GetServiceById(ServiceId);

        [HttpGet("GetServiceByOrderDetailId/{OrderDetailId}")]
        public async Task<Response> GetServiceByOrderDetailId(Guid OrderDetailId, int pageIndex = 0, int pageSize = 10) => await _serviceServices.GetServiceByOrderDetailId(OrderDetailId, pageIndex, pageSize);

        [HttpPut("UpdateService/{ServiceId}")]
        public async Task<IActionResult> UpdateService(Guid ServiceId, UpdateServiceViewModel Service)
        {
            if (ModelState.IsValid)
            {
                ValidationResult result = _validatorUpdate.Validate(Service);
                if (result.IsValid)
                {
                    if (await _serviceServices.UpdateService(ServiceId, Service) != null)
                    {
                        return Ok("Update Service Success");
                    }
                    return BadRequest("Invalid Id");
                }
            }
            return BadRequest("Update Failed,Invalid Input Information");
        }
    }
}
