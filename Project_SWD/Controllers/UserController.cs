﻿using Application.Interfaces;
using Application.ViewModels.UserViewModels;
using Applications.ViewModels.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace Project_SWD.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserService _userService;
        public UserController(IUserService userService)
        {
            _userService = userService;
        }
        [HttpPost("Register")]
        [AllowAnonymous]
        public async Task<Response> Register([FromBody] CreateUserViewModel createUserViewModel) => await _userService.AddUser(createUserViewModel);

        [HttpPost("Login")]
        public async Task<Response> Login(UserLoginViewModel userLoginViewModel)
        {
            var check = _userService.GetLoginUser(userLoginViewModel);
            if(check == true) 
            {
                return new Response(HttpStatusCode.OK, "User Exist");
            }
            else
            {
                return new Response(HttpStatusCode.BadRequest, "User not exist");
            } 
        }
    }
}
