﻿using Application.Commos;
using Application.Interfaces;
using Application.Services;
using Application.ViewModels.OrderDetailViewModels;
using Application.ViewModels.ProductViewModels;
using Applications.ViewModels.Response;
using FluentValidation;
using FluentValidation.Results;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace Project_SWD.Controllers
{
    public class OrderDetailController : Controller
    {
        private readonly IOrderDetailService _orderdetailService;
        private readonly IValidator<CreateOrderDetailViewModel> _validatorCreate;
        private readonly IValidator<UpdateOrderDetailViewModel> _validatorUpdate;

        public OrderDetailController(IOrderDetailService orderdetailService, IValidator<CreateOrderDetailViewModel> validatorCreate, IValidator<UpdateOrderDetailViewModel> validatorUpdate)
        {
            _orderdetailService = orderdetailService;
            _validatorCreate = validatorCreate;
            _validatorUpdate = validatorUpdate;
        }
        [HttpPost("CreateOrderDetail")]
        public async Task<Response> CreateOrderDetail(CreateOrderDetailViewModel OrderDetailModel)
        {
            if (ModelState.IsValid)
            {
                ValidationResult validation = _validatorCreate.Validate(OrderDetailModel);
                if (validation.IsValid)
                {
                    await _orderdetailService.CreateOrderDetail(OrderDetailModel);
                    return new Response(HttpStatusCode.OK, "Create Order Detail Succeed", OrderDetailModel);
                }
            }
            else
            {
                return new Response(HttpStatusCode.BadRequest, "Create Failed, Invalid input");
            }
            return new Response(HttpStatusCode.BadRequest, "Invalid Input");
        }
        [HttpGet("GetAllOrderDetails")]
        public async Task<Pagination<OrderDetailViewModel>> GetAllOrderDetails(int pageIndex = 0, int pageSize = 10) => await _orderdetailService.GetAllOrderDetails(pageIndex, pageSize);

        [HttpGet("GetOrderDetailById/{OrderDetailId}")]
        public async Task<Response> GetOrderDetailById(Guid OrderDetailId) => await _orderdetailService.GetOrderDetailById(OrderDetailId);

        [HttpGet("GetOrderDetailtByProductId/{ProductId}")]
        public async Task<Response> GetProductByProductId(Guid ProductId, int pageIndex = 0, int pageSize = 10) => await _orderdetailService.GetOrderDetailByProductId(ProductId, pageIndex, pageSize);

        [HttpGet("GetProductByOrderId/{OrderId}")]
        public async Task<Response> GetProductByOrderId(Guid OrderId, int pageIndex = 0, int pageSize = 10) => await _orderdetailService.GetOrderDetailByOrderId(OrderId, pageIndex, pageSize);

        [HttpPut("UpdateOrderDetail/{OrderDetailId}")]
        public async Task<IActionResult> UpdateOrderDetail(Guid OrderDetailId, UpdateOrderDetailViewModel OrderDetail)
        {
            if (ModelState.IsValid)
            {
                ValidationResult result = _validatorUpdate.Validate(OrderDetail);
                if (result.IsValid)
                {
                    if (await _orderdetailService.UpdateOrderDetail(OrderDetailId, OrderDetail) != null)
                    {
                        return Ok("Update Order Detail Success");
                    }
                    return BadRequest("Invalid Id");
                }
            }
            return BadRequest("Update Failed,Invalid Input Information");
        }
    }
}
