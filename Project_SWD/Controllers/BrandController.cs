﻿using Application.Commos;
using Application.Interfaces;
using Application.ViewModels.BrandViewModels;
using Applications.ViewModels.Response;
using FluentValidation;
using FluentValidation.Results;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace Project_SWD.Controllers
{
    public class BrandController : ControllerBase
    {
        private readonly IBrandService _brandServices;
        private readonly IValidator<CreateBrandViewModels> _validatorCreate;
        private readonly IValidator<UpdateBrandViewModels> _validatorUpdate;
        public BrandController(IBrandService brandServices,
            IValidator<CreateBrandViewModels> validatorCreate,
            IValidator<UpdateBrandViewModels> validatorUpdate
            )
        {
            _brandServices = brandServices;
            _validatorCreate = validatorCreate;
            _validatorUpdate = validatorUpdate;
        }
        [HttpPost("CreateBrand")]
        public async Task<Response> CreateBrand(CreateBrandViewModels BrandModel)
        {
            if (ModelState.IsValid)
            {
                ValidationResult validation = _validatorCreate.Validate(BrandModel);
                if (validation.IsValid)
                {
                    await _brandServices.CreateBrand(BrandModel);
                    return new Response(HttpStatusCode.OK, "Create Brand Succeed", BrandModel);
                }
            }
            else
            {
                return new Response(HttpStatusCode.BadRequest, "Create Failed, Invalid input");
            }
            return new Response(HttpStatusCode.BadRequest, "Invalid Input");
        }
        [HttpGet("GetAllBrands")]
        public async Task<Pagination<BrandViewModels>> GetAllBrands(int pageIndex = 0, int pageSize = 10) => await _brandServices.GetAllBrands(pageIndex, pageSize);

        [HttpGet("GetBrandById/{BrandId}")]
        public async Task<Response> GetBrandById(Guid BrandId) => await _brandServices.GetBrandById(BrandId);

        [HttpGet("GetBrandByName/{BrandName}")]
        public async Task<Response> GetBrandByName(string BrandName, int pageIndex = 0, int pageSize = 10)
        {
            return await _brandServices.GetBrandByName(BrandName, pageIndex, pageSize);
        }
        [HttpPut("UpdateBrand/{BrandId}")]
        
        public async Task<IActionResult> UpdateBrand(Guid BrandId, UpdateBrandViewModels Brand)
        {
            if (ModelState.IsValid)
            {
                ValidationResult result = _validatorUpdate.Validate(Brand);
                if (result.IsValid)
                {
                    if (await _brandServices.UpdateBrand(BrandId, Brand) != null)
                    {
                        return Ok("Update Brand Success");
                    }
                    return BadRequest("Invalid Id");
                }
            }
            return BadRequest("Update Failed,Invalid Input Information");
        }
    }
}

