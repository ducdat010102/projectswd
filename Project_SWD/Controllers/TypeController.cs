﻿using Application.Commos;
using Application.Interfaces;
using Application.Services;
using Application.ViewModels.TypeViewModels;
using Applications.ViewModels.Response;
using FluentValidation;
using FluentValidation.Results;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace Project_SWD.Controllers
{
    public class TypeController : Controller
    {
        private readonly ITypeService _typeServices;
        private readonly IValidator<CreateTypeViewModel> _validatorCreate;
        private readonly IValidator<UpdateTypeViewModel> _validatorUpdate;
        public TypeController(ITypeService typeServices,
            IValidator<CreateTypeViewModel> validatorCreate,
            IValidator<UpdateTypeViewModel> validatorUpdate
            )
        {
            _typeServices = typeServices;
            _validatorCreate = validatorCreate;
            _validatorUpdate = validatorUpdate;
        }
        [HttpPost("CreateType")]
        public async Task<Response> CreateType(CreateTypeViewModel TypeModel)
        {
            if (ModelState.IsValid)
            {
                ValidationResult validation = _validatorCreate.Validate(TypeModel);
                if (validation.IsValid)
                {
                    await _typeServices.CreateType(TypeModel);
                    return new Response(HttpStatusCode.OK, "Create Type Succeed", TypeModel);
                }
            }
            else
            {
                return new Response(HttpStatusCode.BadRequest, "Create Failed, Invalid input");
            }
            return new Response(HttpStatusCode.BadRequest, "Invalid Input");
        }
        [HttpGet("GetAllTypes")]
        public async Task<Pagination<TypeViewModel>> GetAllTypes(int pageIndex = 0, int pageSize = 10) => await _typeServices.GetAllTypes(pageIndex, pageSize);

        [HttpGet("GetTypeByName/{TypeName}")]
        public async Task<Response> GetTypeByName(string TypeName, int pageIndex = 0, int pageSize = 10)
        {
            return await _typeServices.GetTypeByName(TypeName, pageIndex, pageSize);
        }

        [HttpPut("UpdateType/{TypeId}")]
        public async Task<IActionResult> UpdateType(Guid TypeId, UpdateTypeViewModel Type)
        {
            if (ModelState.IsValid)
            {
                ValidationResult result = _validatorUpdate.Validate(Type);
                if (result.IsValid)
                {
                    if (await _typeServices.UpdateType(TypeId, Type) != null)
                    {
                        return Ok("Update Product Success");
                    }
                    return BadRequest("Invalid Id");
                }
            }
            return BadRequest("Update Failed,Invalid Input Information");
        }
    }
}
