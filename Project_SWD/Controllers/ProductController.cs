﻿using Application.Commos;
using Application.Interfaces;
using Application.Services;
using Application.ViewModels.BrandViewModels;
using Application.ViewModels.ProductViewModels;
using Applications.ViewModels.Response;
using FluentValidation;
using FluentValidation.Results;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace Project_SWD.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductService _productServices;
        private readonly IValidator<CreateProductViewModels> _validatorCreate;
        private readonly IValidator<UpdateProductViewModels> _validatorUpdate;
        public ProductController(IProductService productServices,
            IValidator<CreateProductViewModels> validatorCreate, IValidator<UpdateProductViewModels> validatorUpdate
            )
        {
            _productServices = productServices;
            _validatorCreate = validatorCreate;
            _validatorUpdate = validatorUpdate;
        }
        [HttpPost("CreateProduct")]
        public async Task<Response> CreateProduct(CreateProductViewModels ProductModel)
        {
            if (ModelState.IsValid)
            {
                ValidationResult validation = _validatorCreate.Validate(ProductModel);
                if (validation.IsValid)
                {
                    await _productServices.CreateProduct(ProductModel);
                    return new Response(HttpStatusCode.OK, "Create Product Succeed", ProductModel);
                }
            }
            else
            {
                return new Response(HttpStatusCode.BadRequest, "Create Failed, Invalid input");
            }
            return new Response(HttpStatusCode.BadRequest, "Invalid Input");
        }

        [HttpGet("GetAllProducts")]
        public async Task<Pagination<ProductViewModels>> GetAllProducts(int pageIndex = 0, int pageSize = 10) => await _productServices.GetAllProducts(pageIndex, pageSize);


        [HttpGet("GetPrductByName/{ProductName}")]
        public async Task<Response> GetProductByName(string ProductName, int pageIndex = 0, int pageSize = 10)
        {
            return await _productServices.GetProductByName(ProductName, pageIndex, pageSize);
        }

        [HttpGet("GetProductById/{ProductId}")]
        public async Task<Response> GetProductById(Guid ProductId) => await _productServices.GetProductById(ProductId);

        [HttpGet("GetProductByBrandId/{BrandId}")]
        public async Task<Response> GetProductByBrandId(Guid BrandId, int pageIndex = 0, int pageSize = 10) => await _productServices.GetProductByBrandId(BrandId, pageIndex, pageSize);

        [HttpPut("UpdateProduct/{ProductId}")]
        public async Task<IActionResult> UpdateProduct(Guid ProductId, UpdateProductViewModels Product)
        {
            if (ModelState.IsValid)
            {
                ValidationResult result = _validatorUpdate.Validate(Product);
                if (result.IsValid)
                {
                    if (await _productServices.UpdateProduct(ProductId, Product) != null)
                    {
                        return Ok("Update Product Success");
                    }
                    return BadRequest("Invalid Id");
                }
            }
            return BadRequest("Update Failed,Invalid Input Information");
        }
    }
}
