﻿using Application.Commos;
using Application.Interfaces;
using Application.Services;
using Application.ViewModels.CategoryViewModels;
using Application.ViewModels.OrderViewModels;
using Application.ViewModels.ProductViewModels;
using Applications.ViewModels.Response;
using FluentValidation;
using FluentValidation.Results;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace Project_SWD.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IOrderService _orderService;
        private readonly IValidator<CreateOrderViewModel> _validatorCreate;

        public OrderController(IOrderService orderService,IValidator<CreateOrderViewModel> validatorCreate) 
        {
            _orderService = orderService;
            _validatorCreate = validatorCreate;
        }
        [HttpPost("CreateOrder")]
        public async Task<Response> CreateOrder(CreateOrderViewModel OrderModel)
        {
            if (ModelState.IsValid)
            {
                ValidationResult validation = _validatorCreate.Validate(OrderModel);
                if (validation.IsValid)
                {
                    await _orderService.CreateOrder(OrderModel);
                    return new Response(HttpStatusCode.OK, "Create Order Succeed", OrderModel);
                }
            }
            else
            {
                return new Response(HttpStatusCode.BadRequest, "Create Failed, Invalid input");
            }
            return new Response(HttpStatusCode.BadRequest, "Invalid Input");
        }

        [HttpGet("GetAllOrders")]
        public async Task<Pagination<OrderViewModel>> GetAllOrders(int pageIndex = 0, int pageSize = 10) => await _orderService.GetAllOrders(pageIndex, pageSize);

        [HttpGet("GetOrdersByUserId/{UserId}")]
        public async Task<Response> GetOrderByUserId(Guid UserId, int pageIndex = 0, int pageSize = 10)
        {
            return await _orderService.GetOrderByUserId(UserId, pageIndex, pageSize);
        }
        
        [HttpPut("UpdateOrder/{OrderId}")]
        public async Task<IActionResult> UpdateProduct(Guid OrderId, UpdateOrderViewModel Order)
        {
            if (ModelState.IsValid)
            {
                    if (await _orderService.UpdateOrder(OrderId, Order) != null)
                    {
                        return Ok("Update Product Success");
                    }
                    return BadRequest("Invalid Id");                
            }
            return BadRequest("Update Failed,Invalid Input Information");
        }
    }
}
